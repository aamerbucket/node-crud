var express = require('express');
var bodyparser = require('body-parser');

var app = express();
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:false}));

var carsAPI = require('./controller/cars.controller');
app.use('/api/cars', carsAPI);

app.listen(8080, () => {
    console.log('Server is running on 8080...');
});
