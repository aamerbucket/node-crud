// get the db connection
var db_con = require('../db_config/db_connection');

var express = require('express');

var router = express.Router();

// invoke getConnection method
var connection = db_con.getConnection();

connection.connect();

// import express and router ---- up

router.get('/', (req, res) => {
    // Below method returns error, all records and fields
    connection.query("select * from cars", (err,records,field) => {
        if (err) {
            console.error('Error ', err.message);
        }else{
            res.send(records);
        }
    });
});

//get by id
router.get('/:id', (req, res) => {
    // Below method returns error, all records and fields
    connection.query("select * from cars where id="+req.params.id, (err,records,field) => {
        if (err) {
            console.error('Error ', err.message);
        }else if(records == ''){
            res.send('Record not found');
        }else{
            res.send(records);
        }
    });
});

//create by id
router.post('/', (req, res) => {
    var carName = req.body.carname;
    var year = req.body.year;
    // Below method returns error, and result
    //console.log("INSERT INTO cars (carname, year) VALUES ('" + carName + "','" + year + "')");
    
    connection.query("INSERT INTO cars (carname, year) VALUES ('" + carName + "','" + year + "')" , (err,result) => {
        if (err) {
            console.error('Error while inserting the data ', err.message);
        }else{
            res.send({insert:"success"});
        }
    });
});

// update
router.put('/:id', (req, res) => {
    var id = req.params.id;
    var carName = req.body.carname;
    var year = req.body.year;
    // Below method returns error, and result
    
    var sql = "UPDATE cars SET carname = '"+ carName +"', year = '"+ year +"' WHERE id = '"+id+"'";
    console.log(sql);
    
    connection.query(sql ,(err,result) => {
        if (err) {
            console.error('Error while updating the data ', err.message);
        }else{
            res.send({update:"success"});
        }
    });
});

router.delete('/:id', (req, res) => {
    var id = req.params.id;
    
    var sql = "DELETE from cars WHERE id = '"+id+"'";
    //console.log(sql);
    
    connection.query(sql ,(err,records,fields) => {
        if (err) {
            console.error('Error while deleting the data ', err.message);
        }else{
            res.send({delete:"success"});
        }
    });
});



// export router to use in server.js 
module.exports = router;